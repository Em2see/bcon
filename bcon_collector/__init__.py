__author__ = 'Brand Connector'
__email__ = 'e2m3x4@mail.ru'
__version__ = '0.0.1'

from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

from .cli import cli

__all__ = ['cli']
