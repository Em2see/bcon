import click
import os
import sys
from .producer import process_catalogs
from .producer import process_brands, process_images
from .crawler import Crawler
from .models import Base, engine, Catalog, Session
from loguru import logger
from flask.cli import FlaskGroup
from .server import create_app


@click.group(cls=FlaskGroup, create_app=create_app)
def cli():
    pass


@cli.command()
def prepare_catalogs():
    prepare_catalogs()


@cli.command()
def collect_brands():
    click.echo('run producer for brands')
    process_brands()


@cli.command()
def collect_images():
    click.echo('run producer for images')
    process_images()


@cli.command()
def crawler():
    click.echo('Crawling the sites')
    try:
        Crawler().process()
    except KeyboardInterrupt:
        logger.error('Interrupted')
        try:
            sys.exit(0)
        except SystemExit:
            os._exit(0)


@cli.group()
def db():
    pass


@db.command('fixtures')
@click.pass_obj
def db_fixtures(config):
    # Create all tables in the engine. This is equivalent to "Create Table"
    # statements in raw SQL.
    Base.metadata.drop_all(engine)
    Base.metadata.create_all(engine)
    num = 5
    with Session() as sess:
        sess.add_all([
            Catalog(url=f'https://source.unsplash.com/random/{i}00x200',
                    store_name=f'catalog {i}',
                    category='clothes')
            for i in range(1, num)
        ])
        sess.commit()


@db.command('drop')
@click.pass_obj
def db_drop(config):
    Base.metadata.drop_all(engine)

