import requests
from dagster import get_dagster_logger, job, op


REQUIRED_BRANDS = ["Calvin Klein",
                   "Tommy Hilfiger",
                   "LOEWE",
                   "Lauren Ralph Lauren",
                   "Longchamp",
                   "Paco Rabanne",
                   "Ted Baker",
                   "Tory Burch",
                   "Gucci",
                   "Burberry",
                   "Levi's",
                   "Michael Kors",
                   "Armani",
                   "Emporio Armani",
                   "Nike",]


@op
def prepare_brand_names(context):
    # here
    out = context.op_config["brands"]

    catalog = CatalogApi(requests=session)
    catalog.prepare()
    response_json = catalog.get_listings()
    brands = catalog.parse_brand(response_json)
    brands = {v: k for k, v in brands + [('1207819', 'Armani')]}

    categories = {'men_clothing': '136330', 'women_clothing': '135967'}

    get_dagster_logger().info(f'Brands: {brands}')

    return brands, categories


@op
def get_brands(brands_list):
    pass


@op
def create_new_brands(brands_list):
    catalogs = []
    for brand_name in REQUIRED_BRANDS[::-1]:
        brand_id = brands[brand_name]
        logger.info(brand_name)
        for category in categories:
            category_id = categories[category]
            catalogs.append(Catalog(
                url=json.dumps({'designer': brand_id, 'category': category_id}),
                store_name=f"Farfetch_{brand_name}",
                category=f"{category}"
            ))

    with Session() as sess:
        for catalog in catalogs:
            sess.add(catalog)
        sess.commit()


@op
def collect_items(brands_data):
    pass
