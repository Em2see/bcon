### Brief description


### CLI description:

#### Collector

Collect brands which are in the .env or config.
```bash
bcon collector prepare-catalogs
```

It's instance responsible for executing requests for 
items collecting and image downloading
```bash
bcon collector runner
```

It's instance responsible for creating tasks for items and images.
```bash
bcon collector producer
```

#### ML and server

Run server with latest model
```bash
bcon ml server
```

Run pipeline for model trining.
```bash
bcon ml train
```