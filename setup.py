import os
import sys

from setuptools import find_packages, setup

def_path = "."
package_path = def_path

if package_path not in sys.path:
    sys.path.append(package_path)

from bcon import __author__, __email__, __version__

with open(os.path.join(def_path, "README.md"), "rb") as fh:
    long_description = fh.read().decode("utf-8")

with open(os.path.join(def_path, "requirements.txt"), "r") as fh:
    requirements = fh.readlines()

with open(os.path.join(def_path, "HISTORY.rst"), "rb") as history_file:
    history = history_file.read().decode("utf-8")

# data_files

setup(
    name="bcon",
    version=__version__,
    author=__author__,
    author_email=__email__,
    license="MIT",
    description="Tool for collecting images",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    packages=find_packages(where=def_path),
    package_dir={"": def_path},
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_data={},
    include_package_data=True,
    python_requires=">=3.6",
    cmdclass={},
    entry_points={
        'console_scripts': ['bcon=bcon:cli'],
    },
    install_requires=requirements,
    setup_requires=["wheel"],
    test_suite="tests.my_module_suite",
)
