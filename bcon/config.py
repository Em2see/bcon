import os
from pika import ConnectionParameters
from pika.credentials import PlainCredentials
import pickle


DB_HOST_STRING = "postgresql://{login}:{password}@{host}:{port}/{{schema}}".format(
    login=os.environ['PG_USER'],
    password=os.environ['PG_PASSWORD'],
    host=os.environ['PG_HOST'],
    port=os.environ['PG_PORT']
)

DB_STRING = DB_HOST_STRING.format(
    schema=os.environ['PG_SCHEMA']
)

DB_RESULT_STRING = DB_HOST_STRING.format(
    schema=os.environ['PG_RESULT_SCHEMA']
)
root_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
root_path = os.path.join(root_path, 'db')

# DB_STRING = "sqlite:///" + os.path.join(root_path, 'master.sqlite3')
# DB_RESULT_STRING = "sqlite:///" + os.path.join(root_path, 'results.sqlite3')

FTP_CONFIG = {
    'host': os.environ['FTP_HOST'],
    'port': int(os.environ['FTP_PORT']),
    'user': os.environ['FTP_USER'],
    'passwd': os.environ['FTP_PASSWORD'],
    'passive': os.environ.get('FTP_PASSIVE', True) in [True, 'True', 'true']
}

RABBITMQ_CONFIG = {
    'host': os.environ['RABBITMQ_HOST'],
    'port': os.environ['RABBITMQ_PORT'],
    'virtual_host': os.environ['RABBITMQ_VHOST'],
    'credentials': PlainCredentials(
        username=os.environ['RABBITMQ_USERNAME'],
        password=os.environ['RABBITMQ_PASSWORD']
    ),
    'heartbeat': 600,
    'blocked_connection_timeout': 300,
}

RABBITMQ_CONNECT_PARAMS = ConnectionParameters(**RABBITMQ_CONFIG)

BASE_PATH = os.path.abspath(os.path.join(os.path.dirname(__file__), '..'))

BASE_PATH = os.environ.get('BASE_PATH', BASE_PATH)

DATA_PATH = os.path.join(BASE_PATH, 'data')

# target_shape = 224, 224

target_shape = 512, 512

EMBEDDINGS_PKL = os.path.join(DATA_PATH, 'models', 'embeddings.pkl')
FILES_PKL = os.path.join(DATA_PATH, 'models', 'files.pkl')
KNN_PKL = os.path.join(DATA_PATH, 'models', 'knn.pkl')


class Config(object):
    DEBUG = False
    FLASK_DEBUG = 1
    TESTING = True
    SECRET_KEY = 'Secret!'
    ITEMS_IN_RESPONSE = 10
    NOTIFICATIONS_PER_PAGE = 5

    ROOT_PATH = os.path.join(BASE_PATH)
    MODEL = None
    KNN = None
    FTP_CONFIG = FTP_CONFIG
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = DB_STRING
    SQLALCHEMY_BINDS = {
        'results': DB_RESULT_STRING
    }
    MIGRATIONS_PATH = os.path.join(ROOT_PATH, 'migrations')
    FILE_PATHS = pickle.load(open(FILES_PKL, 'rb')) if os.path.exists(FILES_PKL) else None


class ProductionConfig(Config):
    DEBUG = False


class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = True
