from .core.producer import process_brands, process_images, process_item_info, process_output
from .core.worker import run_worker
from .core.models import Catalog, Item, Result, Proxy
from .core.models_results import tag_results, Tag, Images
from .core import create_app
from .crawler import Crawler
import click
import sys
from loguru import logger
from flask import current_app
from flask.cli import with_appcontext, pass_script_info
from flask.cli import FlaskGroup, get_version as get_flask_version


if sys.version_info >= (3, 10):
    from importlib import metadata
else:
    import importlib_metadata as metadata


def get_version(ctx, param, value):
    if not value or ctx.resilient_parsing:
        return

    from . import __version__
    from .core import __version__ as core_version
    from .crawler import __version__ as crawler_version
    from .ml import __version__ as ml_version

    click.echo(
        (f"Brand connector {__version__}\n" +
         f"Brand core {core_version}\n" +
         f"Brand crawler {crawler_version}\n" +
         f"Brand ML {ml_version}\n"
         ),
        color=ctx.color,
    )
    # import pdb; pdb.set_trace()
    get_flask_version(ctx, param, value)


def get_collector_info(ctx, param, value):
    from .core.utils import get_info_data
    info = get_info_data()
    click.echo(info, color=ctx.color)


@click.group(cls=FlaskGroup, add_version_option=False, create_app=create_app)
@click.option('--version', help="Show the 'Brand connector' version",
              expose_value=False,
              callback=get_version,
              is_flag=True,
              is_eager=True,)
@pass_script_info
@click.pass_context
def cli(ctx, info, **kwargs):
    app_ctx = ctx.obj.load_app().app_context()
    app_ctx.push()
    ctx.call_on_close(app_ctx.pop)


@cli.group()
@click.option('--info', help="Show the 'Brand connector' info",
              expose_value=False,
              callback=get_collector_info,
              is_flag=True,
              is_eager=True,)
@click.pass_context
def collector(ctx):
    logger.info('cli collector')
    from .crawler import RequestSession
    current_app.config['session'] = RequestSession()


# @collector.command()
# @click.pass_context
# def info(ctx):
#     get_collector_info(ctx)

@collector.command()
def prepare_catalogs():
    prepare_catalogs()


@collector.command()
def runner():
    click.echo('run crawler')
    crawler = Crawler()
    click.echo('start consuming')
    run_worker(crawler.get_data)


@collector.command()
@click.option('--all', 'produce_type', flag_value='all', default=True)
@click.option('--brands', 'produce_type', flag_value='brands')
@click.option('--images', 'produce_type', flag_value='images')
@click.option('--output', 'produce_type', flag_value='output')
def producer(produce_type):
    if produce_type in ['all', 'brands']:
        click.echo('run producer for brands')
        process_brands()
    if produce_type in ['all', 'images']:
        click.echo('run producer for images')
        process_item_info()
        process_images()
    if produce_type in ['all', 'output']:
        process_output()


@cli.group()
@with_appcontext
def ml():
    logger.info('cli ML')
    from .ml.knn import load_knn
    from .ml.backbone import prepare_model
    current_app.config['MODEL'] = prepare_model()
    current_app.config['KNN'] = load_knn()


@ml.command()
@click.pass_context
@with_appcontext
def server(ctx):
    from .server.main_bp import main_bp
    current_app.register_blueprint(main_bp, url_prefix='/')
    ctx.forward(cli.commands['run'])


@ml.command()
def train():
    pass


@cli.group()
def db():
    pass


@db.command('fixtures')
@click.pass_obj
def db_fixtures(config):
    # Create all tables in the engine. This is equivalent to "Create Table"
    # statements in raw SQL.
    session = db.session

    # session.metadata.drop_all(engine)
    # session.metadata.create_all(engine)
    num = 5

    session.add_all([
        Catalog(url=f'https://source.unsplash.com/random/{i}00x200',
                store_name=f'catalog {i}',
                category='clothes')
        for i in range(1, num)
    ])
    session.commit()


@db.command('drop')
@click.pass_obj
def db_drop(config):
    session = db.session
    for table in [Catalog, Item, Result, Proxy]:
        table.query.delete()

    for table in [tag_results, Tag, Images]:
        table.query.delete()

    session.commit()
