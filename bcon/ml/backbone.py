import tensorflow as tf
from tensorflow.keras.preprocessing import image
from tensorflow.keras.layers import GlobalMaxPooling2D
from tensorflow.keras.applications.resnet50 import ResNet50, preprocess_input
from tensorflow.keras.applications import resnet
from tensorflow.keras import layers, Model
import numpy as np
from numpy.linalg import norm
import os
from tqdm import tqdm
import pickle
from loguru import logger
from io import BytesIO
from ..config import EMBEDDINGS_PKL, DATA_PATH, KNN_PKL, FILES_PKL
from ..config import target_shape
from ..core.models_results import Images


def extract_features(img_byte_data, model, pad=False, preserve_ratio=False):
    # logger.info(type(img_path))
    img_decoded = tf.io.decode_jpeg(img_byte_data, channels=3)
    if pad:
        img_array = tf.image.resize_with_pad(img_decoded, *target_shape, method='nearest')
    else:
        img_array = tf.image.resize(img_decoded, target_shape, preserve_aspect_ratio=preserve_ratio, method='nearest')

    expanded_img_array = np.expand_dims(img_array, axis=0)
    preprocessed_img = preprocess_input(expanded_img_array)
    result = model.predict(preprocessed_img).flatten()
    normalized_result = result / norm(result)

    return normalized_result


def preprocess_image(filename):
    """
    Load the specified file as a JPEG image, preprocess it and
    resize it to the target shape.
    """

    image_string = tf.io.read_file(filename)
    image = tf.image.decode_jpeg(image_string, channels=3)
    image = tf.image.convert_image_dtype(image, tf.float32)
    image = tf.image.resize(image, target_shape)
    return image


def prepare_model():
    model = ResNet50(weights='imagenet', include_top=False, input_shape=target_shape + (3, ))
    model.trainable = False

    model = tf.keras.Sequential([
        model,
        GlobalMaxPooling2D()
    ])

    return model


def save_all(embeddings, file_paths, knn):
    pickle.dump(embeddings, open(EMBEDDINGS_PKL, 'wb'))
    pickle.dump(file_paths, open(FILES_PKL, 'wb'))
    pickle.dump(knn, open(KNN_PKL, 'wb'))


def get_embeddings(model, data_source='LOCAL'):
    images = Images.query.all()
    file_paths = []

    for image in images:
        path = image.path.split('/')
        path = os.path.abspath(os.path.join(DATA_PATH, *path))
        # logger.info(path)
        if path.endswith('jpg') and os.path.exists(path):
            file_paths.append((image.id, path))

    embedings = []

    for file_id, file_path in tqdm(file_paths):
        with open(file_path, 'rb') as fl:
            embedings.append(extract_features(fl.read(), model))

    return file_paths, embedings


def prepare_model2():
    base_cnn = resnet.ResNet50(
        weights="imagenet", input_shape=target_shape + (3,), include_top=False
    )

    flatten = layers.Flatten()(base_cnn.output)
    dense1 = layers.Dense(512, activation="relu")(flatten)
    dense1 = layers.BatchNormalization()(dense1)
    dense2 = layers.Dense(256, activation="relu")(dense1)
    dense2 = layers.BatchNormalization()(dense2)
    output = layers.Dense(256)(dense2)

    embedding = Model(base_cnn.input, output, name="Embedding")

    trainable = False
    for layer in base_cnn.layers:
        if layer.name == "conv5_block1_out":
            trainable = True
        layer.trainable = trainable
