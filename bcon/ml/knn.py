from ..config import EMBEDDINGS_PKL, DATA_PATH, KNN_PKL, FILES_PKL
from .backbone import extract_features
from sklearn.neighbors import NearestNeighbors
import pickle
from loguru import logger


def train_knn(embeddings, file_paths):
    neighbors = NearestNeighbors(n_neighbors=10, algorithm='brute', metric='euclidean')
    neighbors.fit(embeddings)

    return neighbors


def load_knn():
    try:
        neighbors = pickle.load(open(KNN_PKL, 'rb'))
    except Exception as ex:
        logger.error(ex)

    return neighbors


def predict(neighbors, model, image, file_paths):
    normalized_result = extract_features(image, model)
    distances, indices = neighbors.kneighbors([normalized_result], n_neighbors=20)
    indices = indices.flatten().tolist()
    return [file_paths[i][0] for i in indices]

