from IPython.display import Image, display


def print_image(driver):
    pil_img = Image(data=driver.get_screenshot_as_png())
    display(pil_img)
