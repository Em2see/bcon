from ..core.queue import RabbitMQ
from loguru import logger
from ..config import DATA_PATH
import json
import os
from .store import FTP_Store
from .data_collector import get_catalog_items, update_item_details, process_output_images, collect_item_images
from .requests import RequestSession
from ..config import FTP_CONFIG


class Crawler:
    def __init__(self, session=None):
        if session is None:
            self.session = RequestSession().session

    def save_file(self, file_name, file_data):
        file_path = os.path.join(DATA_PATH, file_name + '.jpg')
        logger.info(f"written {file_path} {len(file_data)} bytes")
        with open(file_path, 'wb') as fl:
            fl.write(file_data)

    def save_ftp_file(self, file_name, file_data):
        file_name = file_name + '.jpg'
        logger.info(f"written {file_name} {len(file_data)} bytes")
        import pdb; pdb.set_trace()
        with FTP_Store(FTP_CONFIG) as ftp:
            ftp.store_file(file_name, file_data)

    def get_data(self, routing_key, properties, body):
        message = body.decode()
        message = json.loads(message)

        logger.info(f"Item id={message['id']} [{routing_key}]")

        if message['type'] == 'info':
            update_item_details(message['id'], self.session)
        elif message['type'] == 'brands':
            get_catalog_items(message['id'], self.session)
        elif message['type'] == 'images':
            collect_item_images(message['id'], self.session)
        elif message['type'] == 'results':
            process_output_images(message['id'], self.session)
