import re
from sqlalchemy import and_, or_
from loguru import logger
from jsonpath_ng import parse, jsonpath
import json
from ..core.models import Catalog, Item, ItemStatus, Result
from ..core.models_results import get_or_create, Tag, TAG_NAME_MAX_LENGTH, Images
from ..core import db
from ..core.utils import log_start_stop
from ..config import FTP_CONFIG
from .farfetch.api import ProductApi, CatalogApi
from .store import FTP_Store
from .requests import RequestSession


REQUIRED_BRANDS = ["Calvin Klein",
                   "Tommy Hilfiger",
                   "LOEWE",
                   "Lauren Ralph Lauren",
                   "Longchamp",
                   "Paco Rabanne",
                   "Ted Baker",
                   "Tory Burch",
                   "Gucci",
                   "Burberry",
                   "Levi's",
                   "Michael Kors",
                   "Armani",
                   "Emporio Armani",
                   "Nike",
                   ]


def prepare_catalogs(session=None):
    if session is None:
        session = RequestSession().session
    catalog = CatalogApi(requests=session)
    catalog.prepare()
    response_json = catalog.get_listings()
    brands = catalog.parse_brand(response_json)
    logger.info(brands)
    brands = {v: k for k, v in brands + [('1207819', 'Armani')]}
    logger.info(brands)
    categories = {'men_clothing': '136330', 'women_clothing': '135967'}
    catalogs = []
    for brand_name in REQUIRED_BRANDS[::-1]:
        brand_id = brands[brand_name]
        logger.info(brand_name)
        for category in categories:
            category_id = categories[category]
            catalogs.append(Catalog(
                url=json.dumps({'designer': brand_id, 'category': category_id}),
                store_name=f"Farfetch_{brand_name}",
                category=f"{category}"
            ))

    sess = db.session
    for catalog in catalogs:
        sess.add(catalog)
    sess.commit()


@log_start_stop
def get_catalog_items(catalog_id, session):
    """
    collecting items from Farfetch catalog.
    Catalog usually is a brand + man or woman clothing type.
    """
    logger.info('start catalog collection')
    catalog_db = Catalog.query.filter(Catalog.id == catalog_id).first()

    params = json.loads(catalog_db.url)
    catalog = CatalogApi(requests=session, **params)
    response_json = catalog.get_listings()
    page_info = catalog.parse_pageinfo(response_json)
    logger.info(page_info)
    products_df = catalog.parse_products(response_json)
    if page_info['totalPages'] > 1:
        for page in range(2, page_info['totalPages'] + 1):
            response_json = catalog.get_listings(page=page)
            if response_json is None:
                continue
            products_df = catalog.parse_products(response_json, products_df)

    products = []
    for row in products_df:
        products.append(Item(
            brand=row['brand'],
            url=row['url'],
            catalog=catalog_db,
            short_description=row['description'],
            description=''
        ))

    sess = db.session
    for p in products:
        sess.add(p)
    sess.commit()


@log_start_stop
def update_item_details(item_ids, session):
    """
    get additional information for Item from Farfetch.
    """
    items = Item.query.filter(Item.id.in_(item_ids)).all()

    for product_db in items:
        params = json.loads(product_db.url)
        product = ProductApi(requests=session, **params)
        response_json = product.get_data()
        product_data = product.parse_product(response_json)

        product_db.images = json.dumps(product_data['images'])
        product_db.description = json.dumps(product_data)
        sess = db.session
        sess.commit()
        # sess.close()


@log_start_stop
def collect_item_images(item_ids, session):
    """
    we are collecting images for item_ids
    """
    items = Item.query.filter(Item.id.in_(item_ids)).all()

    for item in items:
        img_urls, details = item.images, item.description
        img_urls = json.loads(img_urls)
        results = []
        with FTP_Store(FTP_CONFIG) as store:
            for url in img_urls:
                    # import pdb; pdb.set_trace()
                    try:
                        filename = re.findall(r'([^\/]+\.(?:png|jpg))', url)[0]
                        file_dir = item.catalog.store_name
                        file_dir = re.sub('[^0-9a-z_]', '', file_dir.lower())
                        file_path = '/' + file_dir + '/' + filename 
                        png_url = re.findall(r'(^.+\.(?:png|jpg))', url)[0]
                        logger.debug(file_path)
                        logger.debug(png_url)

                        try:
                            store.set_path('/' + file_dir)
                            if not store.file_exists(filename):
                                response = session.get(png_url)

                                store.store_file(filename, response.content)
                        except Exception as ex:
                            logger.error('some issues with storage')
                            logger.error(ex)

                        params = {
                            'details': details,
                            'full_url': url,
                            'png_url': png_url
                        }
                        result = Result(
                            item=item,
                            description=json.dumps(params),
                            params='',
                            color='',
                            stored_path=file_path,
                        )
                        results.append(result)
                    except Exception as ex:
                        logger.debug(ex)
        if results:
            sess = db.session
            sess.add_all(results)
            item.status = ItemStatus.DONE.value
            sess.commit()


def prepare_result(image):
    """
    We are getting details from the Farfetch description of the product(item).
    """
    item = image.item
    response_json = json.loads(item.description)
    short_description = json.loads(item.short_description)
    json_expr = parse('$.details.[description, shortDescription, productId, '
                      'merchantId, gender, genderName, department, colors, ageGroup, styleId]')
    values = json_expr.find(response_json)
    result = {'description': '', 'short_description': '', 'category': '', 'url': '', 'tags': [], 'product_id': 0,
              'brand': '', 'season': '', 'path': ''}
    for v in values:
        if str(v.path) in ['gender', 'genderName', 'department', 'colors', 'ageGroup', 'styleId']:
            try:
                if len(str(v.value)) < TAG_NAME_MAX_LENGTH:
                    tag = get_or_create(Tag, name=f"{v.path}_{v.value}")
                    result['tags'].append(tag)
            except Exception as ex:
                logger.error(ex)
                logger.error('Tag is too long')

        elif str(v.path) in ['description', 'shortDescription', 'productId']:
            t_name = re.sub(r'(?<!^)(?=[A-Z])', '_', str(v.path)).lower()
            result[t_name] = v.value

    result['brand'] = item.brand
    result['path'] = image.stored_path

    json_expr = parse('$.details.richText.*.[*].*.[*].displayOptions.`parent`.items[*].[type, value]')
    highlights = json_expr.find(response_json)

    for h in highlights:
        # print(str(h.path), h.value)
        if str(h.path) == 'type' and h.value != 'text':
            logger.info(h.path, h.value)
        elif str(h.path) == 'value':
            if len(str(h.value)) < TAG_NAME_MAX_LENGTH:
                tag = get_or_create(Tag, name=h.value)
                result['tags'].append(tag)

    product_id, url = short_description['id'], short_description['url']
    result['product_id'] = product_id
    result['url'] = "https://farfetch.com" + url

    return result


@log_start_stop
def process_output_images(result_ids, session):
    """
    We are preparing the images for the output.
    """
    images = Result.query \
        .filter(Result.id.in_(result_ids)) \
        .all()

    for i, image in enumerate(images):
        try:
            res = prepare_result(image)
            logger.info(image.stored_path)
            db.session.add(Images(**res))
        except Exception as ex:
            logger.error(ex)
            db.session.rollback()

    db.session.commit()
