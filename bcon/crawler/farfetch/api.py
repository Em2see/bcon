import requests as py_requests
import pandas as pd
from pandas import json_normalize
from jsonpath_ng import parse, jsonpath
from loguru import logger
import random
import time
import json


DEFAULT_TIMEOUT = 10


class Api:
    def __init__(self, requests=None):
        if not requests:
            requests = py_requests
        self.headers = {
            'Origin': 'https://www.farfetch.com',
            'Referer': 'https://www.farfetch.com/shopping/m/items.aspx',
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                          ' Chrome/75.0.3770.100 Safari/537.36'
        }
        self.requests = requests

    def prepare(self):
        # import pdb; pdb.set_trace()
        response = self.requests.get('https://www.farfetch.com/de/', timeout=DEFAULT_TIMEOUT)
        logger.debug(response)
        if response.status_code != 200:
            raise Exception('Farfetch unavailable')
        response = self.requests.get('https://www.farfetch.com/ChangeLocale/Index?subfolderId=761')
        return response.status_code

    def delay(self):
        delay = random.randint(5, 20)
        time.sleep(delay)


class CatalogApi(Api):
    def __init__(self, **kwargs):
        super().__init__(kwargs.get('requests'))
        self.params = {
            'view': 90,
            #'pagetype': 'Shopping',
            #'pricetype': 'FullPrice',
        }

        self.params.update({k: kwargs[k] for k in kwargs if k != 'requests'})
        self.base_url = r'https://www.farfetch.com/plpslice/listing-api/products-facets'

    def get_listings(self, page=1):
        self.params['page'] = page
        logger.debug('requesting')
        logger.debug(self.params)
        self.delay()
        request = self.requests.get(self.base_url, params=self.params, headers=self.headers)
        response = request.json()
        return response

    def parse_categories(self, response_json):
        json_expr = parse('listingFilters.facets.category.values.[*]')
        matches = json_expr.find(response_json)
        out = []
        for m in matches:
            category = m.value
            out.append((category['value'], category['description']))

        return out

    def parse_brand(self, response_json):
        json_expr = parse('listingFilters.facets.designer.values.[*]')
        matches = json_expr.find(response_json)
        out = []
        for m in matches:
            designer = m.value
            out.append((designer['value'], designer['description']))

        return out

    def parse_pageinfo(self, response_json):
        json_expr = parse('listingPagination')
        matches = json_expr.find(response_json)
        pagenation = None
        if matches:
            pagenation = matches[0].value
        return pagenation

    def product_page_to_df(self, products):
        out = []
        for product in products:
            p = {k: '' for k in ['brand', 'url', 'description']}
            p['brand'] = product.get('brand', {'name': ''}).get('name', '')
            p['url'] = json.dumps({'product_id': product['id']})
            p['description'] = json.dumps(product)
            out.append(p)
        return out

    def parse_products(self, response_json, df=None):
        json_expr = parse('listingItems.items')
        matches = json_expr.find(response_json)
        if matches:
            product_list = matches[0].value
            if df is None:
                df = self.product_page_to_df(product_list)
            else:
                df += self.product_page_to_df(product_list)
        return df


class ProductApi(Api):
    def __init__(self, **kwargs):
        '''
        product_id is required
        :param kwargs:
        '''
        super().__init__(kwargs.get('requests'))
        self.product_id = kwargs['product_id']
        self.base_url = r'https://www.farfetch.com/pdpslice/product/'

    def get_data(self):
        logger.debug('requesting')
        logger.debug(self.base_url + str(self.product_id))
        self.delay()
        request = self.requests.get(self.base_url + str(self.product_id), headers=self.headers)
        # import pdb;pdb.set_trace()
        logger.debug(request.status_code)
        response = request.json()
        return response

    def parse_product(self, response_json, df=None):
        out = dict()

        json_expr = parse('productViewModel.[priceInfo,details]')
        matches = json_expr.find(response_json)
        for match in matches:
            field_name = match.path.fields[0]
            out[field_name] = match.value

        json_expr = parse('productViewModel.images.*.[*].zoom')
        matches = json_expr.find(response_json)
        out['images'] = [m.value for m in matches]

        # if df is None:
        #     df = pd.json_normalize(out)
        # else:
        #     df = pd.concat([df, pd.json_normalize(out)])
        # return df

        return out

