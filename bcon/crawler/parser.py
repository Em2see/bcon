import time


class Parser:
    TIMEOUT_2 = 2
    TIMEOUT_0_200 = 0.2
    MAX_ITERATIONS = 10

    def __init__(self, ext_driver=None):
        if ext_driver:
            self.driver = ext_driver

    @classmethod
    def timeout(cls, duration):
        time.sleep(duration)
