import requests
from .farfetch.api import CatalogApi

USER_AGENT = ('Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
              ' Chrome/75.0.3770.100 Safari/537.36')

USER_AGENT = 'Wget/1.14 (linux-gnu)'

class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class RequestSession(object):
    __metaclass__ = Singleton

    def __init__(self):
        self._session = requests.Session()
        self._session.headers.update({'User-Agent': USER_AGENT})
        self.activated = False
        # import logging
        # logging.basicConfig()
        # logging.getLogger().setLevel(logging.DEBUG)
        # requests_log = logging.getLogger("requests.packages.urllib3")
        # requests_log.setLevel(logging.DEBUG)
        # requests_log.propagate = True

    def _activate(self):
        if not self.activated:
            catalog = CatalogApi(requests=self._session)
            catalog.prepare()
            response_json = catalog.get_listings()
            self.activated = True

    @property
    def session(self):
        self._activate()
        return self._session
