from ..parser import Parser
from loguru import logger
import random
from selenium.webdriver.common.by import By


class TommyHilfigerParser(Parser):
    def get_products_qty(self):
        all_items = 0
        current_items = 0
        try:
            grid_total_element = self.driver.find_element(By.XPATH, "//div[contains(@class, 'gridTotal')]")
            grid_total_element = grid_total_element.find_element(By.XPATH, ".//span[@data-total-count]")
            all_items = grid_total_element.get_attribute('data-total-count')
            current_items = grid_total_element.get_attribute('data-display-count')
        except Exception as ex:
            logger.error('Не нашли количество товаров')
        return current_items, all_items

    def ignore_club(self):
        try:
            modal = self.driver.find_element(By.XPATH, "//div[@aria-modal='true']")

            x_button = modal.find_element(By.XPATH, ".//div[contains(@class, 'pvhOverlayCloseX')]")

            x_button.click()
        except Exception as ex:
            logger.error('Не нашли club modal')

    def check_if_logo_hidden(self):
        try:
            logo = self.driver.find_element(By.XPATH, "//div[contains(@class, 'logo')]//img[@title='Tommy Hilfiger']")
            # check if logo accessible
        except Exception as ex:
            logger.error('logo not seen')

    def scroll_2_bottom(self):
        scroll = random.randint(500, 1200)
        self.driver.execute_script(f'window.scrollBy(0,{scroll})')

    def get_pages(self):
        pass

    def get_products_on_page(self):
        products = []
        try:
            products = self.driver.find_elements(By.XPATH, '//div[]')
        except Exception as ex:
            logger.error('Проблемы с получением элементов на странице')
        return products

    def get_on_next_page(self):
        pass

    def get_products(self):
        current_items, all_items = self.get_products_qty()
        collected_items_all = []

        while len(collected_items_all) < all_items:
            iteration = 0
            collected_items = []
            current_items, _ = self.get_products_qty()

            while (len(collected_items) < current_items) or (iteration > self.MAX_ITERATIONS):
                products_on_page = self.get_products_on_page()
                if len(products_on_page) == 0:
                    logger.error('Мы не нашли новых продуктов')
                self.scroll_2_bottom()
                collected_items += products_on_page
                iteration += 1

            collected_items_all += collected_items
            self.get_on_next_page()

    def check_modal_opened(self):
        pass

    def preparations(self):
        self.driver.get('https://usa.tommy.com/en')
        self.timeout(Parser.TIMEOUT_2)
        self.ignore_club()
