from ftplib import FTP, all_errors
from loguru import logger
from io import BytesIO
import os


class FTP_Store:
    def __init__(self, config):
        self.ftp = FTP()
        # self.ftp.set_debuglevel(2)
        self.ftp.connect(host=config['host'], port=config['port'])
        self.ftp.login(user=config['user'], passwd=config['passwd'])
        # import pdb; pdb.set_trace()
        self.ftp.set_pasv(config['passive'])
        # logger.info(f"set dir")
        # self.set_dir('test_data')

    def set_path(self, path):
        try:
            self.ftp.cwd(path)
            return
        except all_errors as ex:
            logger.error(ex)
        try:
            self.ftp.cwd(os.sep)
            for folder in path.split(os.sep)[1:]:
                self.set_dir(folder)
        except all_errors as ex:
            # import pdb; pdb.set_trace()
            logger.error(ex)

    def set_dir(self, folder_name):
        if folder_name not in self.ftp.nlst():
            self.ftp.mkd(folder_name)
        self.ftp.cwd(folder_name)

    def file_exists(self, file_name):
        return file_name in self.ftp.nlst()

    def store_file(self, file_name, binary_data):
        fp = BytesIO(binary_data)
        logger.info(f'storing ftp file {file_name}')
        # STOR
        self.ftp.storbinary(f'APPE {file_name}', fp, 128)

    def read_file(self, file_name):
        with BytesIO() as fp:
            self.ftp.retrbinary('RETR ' + file_name, fp.write)
            data = fp.getvalue()

        return data

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        self.ftp.quit()
