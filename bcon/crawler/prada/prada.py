import time
import random
from loguru import logger
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from ..store import FTP_Store
import re
from ..parser import Parser


class PradaParser(Parser):

    def agree_with_cookies(self):
        # cookie notice
        # 'Я даю согласие'
        # 'I Agree'
        try:
            cookie_agree_modal = self.driver.find_element(By.XPATH,
                                                     "//div[contains(@class, 'cookie') and contains(@class, 'showCookie')]")
            cookie_agree_btn = cookie_agree_modal.find_element(By.XPATH,
                                                               ".//div[text()='I Agree' or text()='Я даю согласие']")
            cookie_agree_btn.click()
        except:
            logger.error('Не нашли cookie modal')

    def choose_world(self):
        try:
            location_btn = self.driver.find_element(By.XPATH,
                                               "//button[@type='submit' and descendant::span[text()='Rest of the World']]")
            location_btn.click()
        except:
            logger.error('Не нашли world modal')

    def check_modal_opened(self):
        self.agree_with_cookies()
        time.sleep(Parser.TIMEOUT_0_200)
        self.choose_world()
        time.sleep(Parser.TIMEOUT_0_200)

    def preparations(self):
        self.driver.get('https://www.prada.com/ww/en/')
        time.sleep(Parser.TIMEOUT_2)
        self.agree_with_cookies()
        time.sleep(Parser.TIMEOUT_2)
        self.choose_world()
        time.sleep(Parser.TIMEOUT_2)

    def parse_catalog_page(self, products):
        init_prods = len(products)

        prod_elements = self.driver.find_elements(By.XPATH, "//div[@data-price]")
        div_params = ['data-product-id', 'data-part-number',
                      'data-price', 'data-on-sale',
                      'data-size-list', 'data-color']
        a_params = ['aria-label', 'href']

        for prod_element in prod_elements:
            product = dict()
            for p in div_params:
                try:
                    product[p] = prod_element.get_attribute(p)
                except:
                    product[p] = ''
            link = prod_element.find_element(By.XPATH, ".//a[@aria-label and contains(@href, 'products')]")
            for p in a_params:
                try:
                    product[p] = link.get_attribute(p)
                except:
                    product[p] = ''
            if product['data-product-id'] not in products:
                products[product['data-product-id']] = product

        try:
            next_page_link = self.driver.find_element(By.XPATH, "//a[descendant::span[text()='Discover more']]")
            next_page_link.click()
            next_page_link = next_page_link.get_attribute('href')
        except:
            scroll = random.randint(500, 1200)
            self.driver.execute_script(f'window.scrollBy(0,{scroll})')
            next_page_link = 1 if len(products) > init_prods else None

        return next_page_link

    def parse_catalog(self, catalog_url):
        parsing = True
        next_page = catalog_url
        products = dict()
        while parsing:
            if isinstance(next_page, str):
                self.driver.get(next_page)
            time.sleep(Parser.TIMEOUT_2)
            next_page = self.parse_catalog_page(products)
            logger.debug(f'qty products {len(products)}')
            parsing = next_page is not None
        return products

    def loading_images(self):
        # resolve preload
        # wonderful next arrows

        close_cross = None
        try:
            next_arrow = self.driver.find_element(By.XPATH, "//a[contains(@class, 'pDetails__arrows--next')]")

            if not next_arrow.is_displayed():
                pictures = self.driver.find_elements(By.XPATH,
                                               "//div[contains(@class, 'pDetails__slide')]//img[contains(@class, 'pDetails__img')]")
                for picture in pictures:
                    if picture.is_displayed():
                        break
                picture.click()

                next_arrow = self.driver.find_element(By.XPATH,
                                                 "//a[contains(@class, 'zoom__arrow') and contains(@class, '--next')]")
                close_cross = self.driver.find_element(By.XPATH, "//a[contains(@class, 'zoom__close')]")

            repeats = 7
            while repeats > 0:
                next_arrow.click()
                time.sleep(Parser.TIMEOUT_2)
                if '--disable' in next_arrow.get_attribute('class'):
                    repeats = 0
                repeats -= 1

            if close_cross:
                close_cross.click()
        except:
            logger.debug('issue with next_arrow')

    def parse_product_page_dropdowns(self, color_selector, color_options):
        try:
            current_value = color_selector.find_element(By.XPATH, ".//a[contains(@class, 'customSelect__input')]/span")
            current_value = current_value.text
        except:
            logger.debug('issue with getting current_value')
            current_value = None

        if not len(color_options):
            color_selector.click()

            options = color_selector.find_elements(By.XPATH, './/ul//li[@id]/a/span')
            for option in options:
                try:
                    if option.text and option.text.lower().strip() not in ['selected', 'color']:
                        color_options.append(option.text)
                except:
                    logger.debug('issue with option')
            color_selector.click()

        self.loading_images()

        images = self.driver.find_elements(By.XPATH, "//img[contains(@class, 'js-picture-image')]")
        img_urls = []

        for img in images:
            img_urls.append(img.get_attribute('src'))

        return img_urls, current_value

    def product_page_dropdowns(self):
        details = self.driver.find_element(By.XPATH, "//section[@id='pdp_details']")
        details = details.text
        img_urls = dict()
        color_options = []

        while True:
            color_selector = self.driver.find_element(By.XPATH, "//div[contains(@class, 'pDetails__color') and descendant::a[contains(@aria-label, 'Color')]]")

            img_urls_list, current_value = self.parse_product_page_dropdowns(color_selector, color_options)
            img_urls[current_value] = img_urls_list
            logger.debug(current_value)
            logger.debug(color_options)
            # import pdb; pdb.set_trace()
            if current_value in color_options:
                color_options.remove(current_value)

            if not color_options:
                break

            try:
                color_selector.click()

                option_elements = color_selector.find_elements(By.XPATH, f".//ul//li[@id]/a/span")
                for option_element in option_elements:
                    if option_element.text == color_options[0]:
                        break

                option_element.click()
                time.sleep(Parser.TIMEOUT_2)

            except:
                pass

        return details, img_urls

    def load_scroll_images(self):
        scroll = True
        while scroll:
            try:
                scroll_btn = self.driver.find_element(By.XPATH, "//a[contains(@class, 'scroll-top-btn')]")
                scroll_btn.click()
                scroll = False
            except:
                scroll = random.randint(500, 1200)
                self.driver.execute_script(f'window.scrollBy(0,{scroll})')
                scroll = True


    def get_options(self, color_button):
        color_button.click()
        elements_options = self.driver.find_elements(By.XPATH, "//div[contains(@class, 'list-color__option')]")
        options = dict()
        txt_options = []

        for opt in elements_options:
            span_element = opt.find_element(By.XPATH, './/span')
            a_element = opt.find_element(By.XPATH, './/a')
            if '--selected' in opt.get_attribute('class'):
                current = span_element.text
            if span_element.text not in options:
                txt_options.append(span_element.text)
                options[span_element.text] = a_element
        # cross

        close_button = self.driver.find_element(By.XPATH, "//div[contains(@class, '__handle-close')]")
        close_button.click()

        return options, txt_options, current

    def product_page_modals(self):
        details = self.driver.find_element(By.XPATH, "//div[contains(@class, 'product-description__container')]")
        details = details.text
        color_button = self.driver.find_element(By.XPATH, "//button[@name='Select color']")
        options, txt_options, current = self.get_options(color_button)

        img_urls = []

        while len(txt_options):
            self.load_scroll_images()

            images = self.driver.find_elements(By.XPATH, "//img[contains(@class, 'js-picture-image')]")

            for img in images:
                img_urls.append(img.get_attribute('src'))

            if current in txt_options:
                txt_options.remove(current)

            if len(txt_options):
                current = txt_options[0]
                break

            color_button.click()
            options[current].click()

        return details, img_urls

    def product_page(self, page_url, item):
        self.driver.get(page_url)
        time.sleep(Parser.TIMEOUT_2)
        self.check_modal_opened()
        try:
            selectors = self.driver.find_element(By.XPATH, "//div[contains(@class, 'pDetails__conf')]")
        except:
            selectors = None
        if selectors:
            details, img_urls = self.product_page_dropdowns()
        else:
            details, img_urls = self.product_page_modals()

        return details, img_urls



