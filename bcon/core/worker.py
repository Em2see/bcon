from .queue import ReconnectingConsumer
from .producer import PUBLISHER_CONFIG


def run_worker(callback):
    PUBLISHER_CONFIG['callback'] = callback
    consumer = ReconnectingConsumer(**PUBLISHER_CONFIG)
    consumer.run()
