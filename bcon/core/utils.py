from itertools import islice
from functools import wraps
from loguru import logger
from .models import Item, Catalog, Result
from sqlalchemy import and_, func
from .models import db


def batch(iterable, size):
    source_iter = iter(iterable)
    while True:
        batch = list(islice(source_iter, size))
        if len(batch):
            yield batch
        else:
            return


def log_start_stop(f):
    @wraps(f)
    def wrapper(*args, **kwds):
        logger.info(f"Start {f.__name__} args({args}) kwds({kwds})")
        res = f(*args, **kwds)
        logger.info(f"Stop {f.__name__}")
        return res
    return wrapper


def get_info_data():
    #
    # get all images qty, errors images
    #
    out = ["Farfetch items"]
    # get all items qty
    qs = db.session.query(Item).join(Catalog).filter(Catalog.store_name.like('Farfetch'))
    out.append(f"Total items {qs.count()}")

    # items to collect info
    qs = db.session.query(Item).join(Item.catalog).filter(
        and_(
            Item.description == '',
            Catalog.store_name.contains('Farfetch'))
    )
    out.append(f"Items to collect info {qs.count()}")

    # New items which we should collect
    img_qty = func.coalesce(func.length(Item.images) - func.length(func.replace(Item.images, ',', '')) + 1, 0).label(
        'img_qty')
    img_count = func.count(Result.id).label('img_count')

    qs = db.session \
        .query(Item.id, img_qty, img_count) \
        .join(Result, isouter=True) \
        .filter(and_(Item.description != '', Item.catalog_id > 6)) \
        .group_by(Result.item_id, Item.id) \
        .having(img_count == 0)
    out.append(f"Items to collect images {qs.count()}")

    # unique images
    qs = db.session.query(Result).distinct(Result.stored_path)
    out.append(f"Unique images {qs.count()}")

    return "\n".join(out)
