from flask import Flask
from loguru import logger
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy


migrate = Migrate()
db = SQLAlchemy()


def create_app(*args, **kwargs):
    app = Flask(__name__, instance_relative_config=True)
    logger.info("creating app")
    app.config.from_object("bcon.config.DevelopmentConfig")
    db.init_app(app)
    migrate.init_app(app, db)
    return app
