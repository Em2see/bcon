from .app import db


MAX_NAME_LEN = 250
TAG_NAME_MAX_LENGTH = 128


tag_results = db.Table('tag_results', db.metadata,
                       db.Column('tag_id', db.ForeignKey('tags.id', ondelete='CASCADE')),
                       db.Column('image_id', db.ForeignKey('images.id', ondelete='CASCADE'))
                       )


class Tag(db.Model):
    __bind_key__ = 'results'
    __tablename__ = 'tags'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(MAX_NAME_LEN), nullable=False, unique=True)

    def __str__(self):
        return self.name

    def __repr__(self):
        return f'"{self.name}"'


def get_or_create(model, **kwargs):
    instance = model.query.filter_by(**kwargs).first()
    if instance:
        return instance
    else:
        instance = model(**kwargs)
        db.session.add(instance)
        db.session.commit()
        return instance


class Images(db.Model):
    __bind_key__ = 'results'
    __tablename__ = 'images'
    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.BigInteger, nullable=False, index=True)
    description = db.Column(db.String(2048), nullable=True)
    short_description = db.Column(db.String(2048), nullable=True)
    category = db.Column(db.String(MAX_NAME_LEN), nullable=False)
    brand = db.Column(db.String(MAX_NAME_LEN), nullable=False)
    path = db.Column(db.String(MAX_NAME_LEN), nullable=False)
    url = db.Column(db.String(MAX_NAME_LEN), nullable=False)
    color = db.Column(db.String(2048), nullable=False, default='')
    season = db.Column(db.String(MAX_NAME_LEN), nullable=False)
    tags = db.relationship('Tag',
                           secondary=tag_results,
                           lazy=True,
                           passive_deletes=True,
                           backref=db.backref('results', lazy=True)
                           )
    __table_args__ = (db.UniqueConstraint('product_id', 'brand', 'path', name='_product_uc'),
                      )
