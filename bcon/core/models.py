from .app import db
from datetime import datetime
from enum import Enum


def now():
    return datetime.now()


class ItemStatus(Enum):
    NEW = 0
    ERROR = 2
    DONE = 3


class Catalog(db.Model):
    __tablename__ = 'catalog'
    id = db.Column(db.Integer, primary_key=True)
    store_name = db.Column(db.String(250), nullable=False)
    url = db.Column(db.String(250), nullable=False)
    category = db.Column(db.String(250), nullable=False)
    priority = db.Column(db.Integer, nullable=False, default=0)


class Item(db.Model):
    __tablename__ = 'item'
    id = db.Column(db.BigInteger, primary_key=True)
    catalog_id = db.Column(db.Integer, db.ForeignKey('catalog.id'), nullable=False, index=True)
    catalog = db.relationship(
        Catalog,
        backref=db.backref('items',
                           uselist=True,
                           cascade='delete,all'),
        foreign_keys=[catalog_id])
    url = db.Column(db.String(250), nullable=False)
    priority = db.Column(db.Integer, nullable=False, default=0)
    brand = db.Column(db.String(250), nullable=False)
    description = db.Column(db.String(32768), nullable=False)
    short_description = db.Column(db.String(32768), nullable=True)
    images = db.Column(db.String(4096), nullable=True)
    status = db.Column(db.Integer, nullable=False, default=ItemStatus.NEW.value)


class Result(db.Model):
    __tablename__ = 'result'
    id = db.Column(db.Integer, primary_key=True)
    item_id = db.Column(db.BigInteger, db.ForeignKey('item.id'), nullable=False, index=True)
    item = db.relationship(
        Item,
        lazy='select',
        backref=db.backref('results',
                           uselist=True,
                           cascade='delete,all'))
    description = db.Column(db.String(8192), nullable=False)
    params = db.Column(db.String(8192), nullable=False)
    color = db.Column(db.String(250), nullable=False)
    stored_path = db.Column(db.String(250), nullable=False)
    status = db.Column(db.Integer, nullable=False, default=ItemStatus.NEW.value)


class Proxy(db.Model):
    __tablename__ = 'proxy'
    id = db.Column(db.Integer, primary_key=True)
    url = db.Column(db.String(250), nullable=False)
    last_check = db.Column(db.DateTime, nullable=False, default=now())
    availability = db.Column(db.Integer, nullable=False, default=0)
