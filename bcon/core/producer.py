from loguru import logger
from sqlalchemy import and_, or_, func
from pika import BasicProperties
from .models import Catalog, Item, Result, ItemStatus, db
from .queue import RabbitMQ
from .queue import Publisher
from .utils import batch
from ..config import RABBITMQ_CONNECT_PARAMS
import json


FARFETCH_STORE_NAME = 'Farfetch'
DEFAULT_STORE_NAME = FARFETCH_STORE_NAME
ITEM_IMGS_BATCH_SIZE = 10
ITEM_INFO_BATCH_SIZE = 5
OUTPUT_BATCH_SIZE = 5
DEFAULT_RESULT_PRIORITY = 1
PUBLISH_INTERVAL = 30

PUBLISHER_CONFIG = {
    'url': RABBITMQ_CONNECT_PARAMS,
    'publish_interval': PUBLISH_INTERVAL
}


class Callback:
    def __init__(self, qs, msg_type='catalog', batch_size=1, post_filter=None):
        self.msg_type = msg_type
        self.batch_size = batch_size
        self.qs = qs
        self.post_filter = post_filter
        self.count = qs.count()
        self.current = 0

    def __call__(self, exchange_name, routing_key):
        ids = []
        try:
            # import pdb; pdb.set_trace()
            while (self.current < self.count) and (len(ids) < self.batch_size):
                self.current += self.batch_size
                res = self.qs.slice(self.current, self.current + self.batch_size).all()
                if self.post_filter:
                    res = [i for i in res if self.post_filter(i)]
                ids += [i.id for i in res]
                # self.current += len(res)
        except:
            pass

        if len(ids) == 0:
            return None

        if self.batch_size == 1:
            ids = ids[0]
        logger.info(ids)
        return {
            'exchange': exchange_name,
            'routing_key': routing_key,
            'body': json.dumps({
                'type': self.msg_type,
                'id': ids
            }),
            'properties': BasicProperties(
                delivery_mode=2,  # make message persistent
            )
        }


def run_publisher(msg_type, store_name=DEFAULT_STORE_NAME):
    batch_size = 1
    post_filter = None
    if msg_type == 'brands':
        qs = Catalog.query.filter(Catalog.store_name.contains(store_name))
    elif msg_type == 'images':
        img_qty = func.coalesce(func.length(Item.images) - func.length(func.replace(Item.images, ',', '')) + 1, 0).label('img_qty')
        img_count = func.count(Result.id).label('img_count')

        qs = db.session\
            .query(Item.id, img_qty, img_count)\
            .join(Result, isouter=True)\
            .filter(and_(Item.description != '', Item.catalog_id > 6))\
            .group_by(Result.item_id, Item.id)\
            .having(img_count == 0)  # img_count != img_qty

        def post_filter(item):
            return item.img_count == 0

        batch_size = ITEM_IMGS_BATCH_SIZE
    elif msg_type == 'info':
        qs = Item.query.join(Item.catalog).filter(
            and_(
                Item.description == '',
                Catalog.store_name.contains(store_name))
        )
        batch_size = ITEM_INFO_BATCH_SIZE
    elif msg_type == 'results':
        qs = Result.query \
            .filter(Result.color == '') \
            .filter(Result.status == ItemStatus.DONE.value) \
            .distinct('stored_path')
        batch_size = OUTPUT_BATCH_SIZE

    publisher = Publisher(**PUBLISHER_CONFIG)
    publisher.set_callback(Callback(qs, msg_type=msg_type, batch_size=batch_size, post_filter=post_filter))
    publisher.run()


def process_brands(store_name=DEFAULT_STORE_NAME):
    run_publisher('brands', store_name)


def process_item_info(store_name=DEFAULT_STORE_NAME):
    run_publisher('info', store_name)


def process_images(store_name=DEFAULT_STORE_NAME):
    run_publisher('images', store_name)


def process_output():
    run_publisher('results')


def get_images_quantity(store_name=DEFAULT_STORE_NAME):
    out = []

    qs = Catalog.query.filter(Catalog.store_name.contains(store_name))
    total = 0
    for catalog in qs.all():
        total += len(catalog.items)
        out.append(str(len(catalog.items)) + " " + str(catalog.store_name))
    out.append(str(total))

    return "\n".join(out)
