# -*- coding: utf-8 -*-
# pylint: disable=C0111,C0103,R0205
from pika import BlockingConnection, BasicProperties
from loguru import logger
import json
from ...config import RABBITMQ_CONNECT_PARAMS


class RabbitMQ:
    exchange_name = 'main_exchange'
    queue_name = 'test'
    
    def __init__(self):
        logger.info(RABBITMQ_CONNECT_PARAMS)
        self.connection = BlockingConnection(RABBITMQ_CONNECT_PARAMS)
        self.channel = self.connection.channel()
        self.channel.exchange_declare(exchange=RabbitMQ.exchange_name, exchange_type='direct')
        self.create_queues()
        self.start_consuming = self.channel.start_consuming

    def create_queues(self):
        result = self.channel.queue_declare(queue=RabbitMQ.queue_name)
        self.queue_name = result.method.queue
        logger.debug(f'queue_name - {self.queue_name}')
        self.channel.queue_bind(exchange=RabbitMQ.exchange_name, queue=self.queue_name)

    def publish(self, severity, message):
        if not isinstance(message, str):
            message = json.dumps(message)
        logger.debug(f'publish msg {message}')
        self.channel.basic_publish(
            exchange=RabbitMQ.exchange_name,
            routing_key=self.queue_name,  # str(severity),
            body=message,
            properties=BasicProperties(
                delivery_mode=2,  # make message persistent
            )
        )

    def set_callback(self, callback):
        self.channel.basic_consume(
            queue=self.queue_name,
            on_message_callback=callback,
            auto_ack=False
        )

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        try:
            if not self.connection.is_closed:
                self.connection.close()
        except ConnectionResetError as ex:
            logger.error(ex)
