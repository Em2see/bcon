from marshmallow import Schema, fields


class ImageSchema(Schema):
    id = fields.Int(dump_only=True)
    product_id = fields.Integer()
    category = fields.Str()
    brand = fields.Str()
    path = fields.Str()
    url = fields.Str()
    color = fields.Str()
    season = fields.Str()
    tags = fields.Function(lambda x: [tag.name for tag in x.tags])
