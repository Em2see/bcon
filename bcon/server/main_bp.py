from ..ml.knn import predict
from ..crawler.store import FTP_Store
from ..models import Session
from ..models_results import Images
from ..serializers import ImageSchema
import random
from loguru import logger
from flask import Blueprint
from sqlalchemy.sql.expression import func
from flask import jsonify, request

main_bp = Blueprint('main_bp', __name__)


def process_image():
    pass


@main_bp.record
def record_params(setup_state):
    app = setup_state.app
    main_bp.model = app.config['MODEL']
    main_bp.config = {key: value for key, value in app.config.items() if key not in ['MODEL']}


@main_bp.route('/', methods=['GET'])
def index_get():
    result_schema = ImageSchema(many=True)
    with Session() as sess:
        results = result_schema.dump(sess.query(Images).limit(main_bp.config['ITEMS_IN_RESPONSE']).all())

    return jsonify({
        'images': random.choices(results, k=10)})


@main_bp.route('/', methods=['POST'])
def index_post():
    content_types = [
        "application/octet-stream",
        "image/jpeg"
    ]
    if request.content_type in content_types:
        data = request.get_data()
        image_ind = predict(main_bp.config['KNN'], main_bp.model, data, main_bp.config['FILE_PATHS'])

        logger.info(image_ind)
        result_schema = ImageSchema(many=True)
        with Session() as sess:
            results = sess.query(Images)\
                    .filter(Images.id.in_(image_ind))\
                    .limit(main_bp.config['ITEMS_IN_RESPONSE'])\
                    .all()
            results = result_schema.dump(results)

        return jsonify({'images': results})


@main_bp.route('/image', methods=['GET'])
def get_image():
    with Session() as sess:
        image = sess.query(Images).order_by(func.random()).limit(1).first()
        path = image.path

    with FTP_Store(config=main_bp.config['FTP_CONFIG']) as ftp:
        data = ftp.read_file(path)

    logger.info(data)

    return data
