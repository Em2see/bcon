FROM python:3.7-stretch

MAINTAINER "Dmitry Petrov"

WORKDIR /code
COPY requirements.txt /code/

# RUN apt-get update

RUN pip install -r requirements.txt \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /usr/share/doc && rm -rf /usr/share/man \
    && apt-get clean

COPY . /code/

RUN pip install . \
    && rm -rf /root/.cache

CMD gunicorn --bind localhost:5000 "bcon.cli:create_app()" 
